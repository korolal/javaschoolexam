package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    private List<Integer> input;
    private int pyramidHeight;

    private int[][] result;

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) {
        input = inputNumbers;
        validateInputNumbers();
        getPyramidHeight();
        input.sort(Comparator.naturalOrder());
        generateTemplate();
        fillTemplate();
        return result;
    }

    private void validateInputNumbers() {
        if (input == null || input.size() == 0 || input.contains(null))
            throw new CannotBuildPyramidException();
    }

    private void getPyramidHeight() {
        pyramidHeight = calculatePyramidHeight();
    }

    //algo description - https://stackoverflow.com/a/295678
    private static boolean isPerfectSquare(long n) {
        if (n < 0)
            return false;

        switch ((int) (n & 0xF)) {
            case 0:
            case 1:
            case 4:
            case 9:
                long tst = (long) Math.sqrt(n);
                return tst * tst == n;

            default:
                return false;
        }
    }

    //math details - https://math.stackexchange.com/a/466653
    private int calculatePyramidHeight() {
        int tempValue = 1 + 8 * input.size();
        if (!isPerfectSquare(tempValue))
            throw new CannotBuildPyramidException();
        return (int) (-1 + Math.sqrt(tempValue)) / 2;
    }

    private void generateTemplate() {
        result = new int[pyramidHeight][2 * pyramidHeight - 1];
    }

    private void fillTemplate() {
        int counter = 0;
        for (int i = 0; i < pyramidHeight; i++) {
            for (int j = 0; j <= i; j++) {
                int x = i;
                int y = pyramidHeight - i - 1 + 2 * j;
                result[x][y] = input.get(counter++);
            }
        }
    }

}
