package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {

    private static Map<String, Integer> priorityMap;
    private static Set<String> delimiterSet;
    private static Set<String> operatorSet;

    private static final String LEFT_BRACE = "(";
    private static final String RIGHT_BRACE = ")";
    private static final String SPACE = " ";
    private static final String PLUS = "+";
    private static final String MINUS = "-";
    private static final String MULT = "*";
    private static final String DIVIDE = "/";
    private static final String EMPTY_STRING = "";
    private static final String DIVIDERS = "+-*/() ";

    static {
        priorityMap = new HashMap<>();
        delimiterSet = new HashSet<>(Arrays.asList(LEFT_BRACE, RIGHT_BRACE, SPACE));
        operatorSet = new HashSet<>(Arrays.asList(PLUS, MINUS, MULT, DIVIDE));
        priorityMap.put(LEFT_BRACE, 1);
        priorityMap.put(MINUS, 2);
        priorityMap.put(PLUS, 2);
        priorityMap.put(MULT, 3);
        priorityMap.put(DIVIDE, 3);
    }

    private enum TokenType {
        SPACE,
        OPERAND,
        OPERATOR,
        DELIMITER
    }

    private static class PolishStackData {
        List<String> resultList = new ArrayList<>();
        Deque<String> stack = new ArrayDeque<>();
        StringTokenizer tokenizer;

        public PolishStackData(String input, String params) {
            tokenizer = new StringTokenizer(input, params, true);
        }

        public void initTokenizer(String input, String params) {
            tokenizer = new StringTokenizer(input, params, true);
        }

        public boolean tokenizerHasMoreToken() {
            return tokenizer.hasMoreTokens();
        }

        public String tokenizerNextToken() {
            return tokenizer.nextToken();
        }

        public void addResultList(String token) {
            resultList.add(token);
        }

        public void pushStack(String token) {
            stack.push(token);
        }

        public String peekStack() {
            return stack.peek();
        }

        public void fromStackToResultList() {
            resultList.add(stack.pop());
        }

        public boolean isStackEmpty() {
            return stack.isEmpty();
        }

        public String stackPop() {
            return stack.pop();
        }

        public List<String> returnResultList() {
            return resultList;
        }
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        if (statement == null || Objects.equals(statement, EMPTY_STRING))
            return null;
        String stringResult;
        try {
            Double doubleResult = calc(parse(statement));
            stringResult = getResultFormatted(doubleResult);
        } catch (CalculatorParseException | CalculateException cpe) {
            System.out.println(cpe.getMessage());
            return null;
        }
        return stringResult;
    }

    private String getResultFormatted(Double doubleResult) {
        String stringResult;
        if (doubleResult == Math.floor(doubleResult)) {
            stringResult = String.valueOf(doubleResult.intValue());
        } else {
            stringResult = String.valueOf(doubleResult);
        }
        return stringResult;
    }

    private static boolean isDelimiter(String token) {
        return delimiterSet.contains(token);
    }

    private static boolean isOperator(String token) {
        return operatorSet.contains(token);
    }

    private static int priority(String token) throws CalculatorParseException {
        int priority = priorityMap.get(token);
        if (token == null)
            throw new CalculatorParseException("wrong priority item !");
        return priority;
    }

    private static TokenType typeRecognizer(String token) {
        if (token.equals(SPACE))
            return TokenType.SPACE;
        if (isOperator(token))
            return TokenType.OPERATOR;
        if (isDelimiter(token))
            return TokenType.DELIMITER;
        return TokenType.OPERAND;
    }

    private static List<String> parse(String input) throws CalculatorParseException {
        PolishStackData pSD = new PolishStackData(input, DIVIDERS);
        String prev = EMPTY_STRING;
        String curr;
        while (pSD.tokenizerHasMoreToken()) {
            curr = pSD.tokenizerNextToken();
            switch (typeRecognizer(curr)) {
                case SPACE:
                    continue;

                case OPERAND:
                    operandAction(pSD, curr);
                    break;

                case DELIMITER:
                    delimiterAction(pSD, curr);
                    break;

                case OPERATOR:
                    operatorAction(pSD, prev, curr);
                    break;
            }
            if (isOperator(prev) && isOperator(curr))
                throw new CalculatorParseException("two operands in a row operand");
            prev = curr;
        }
        collectResultStack(pSD);
        return pSD.returnResultList();
    }

    private static void collectResultStack(PolishStackData pSD) throws CalculatorParseException {
        while (!pSD.isStackEmpty()) {
            if (isOperator(pSD.peekStack()))
                pSD.fromStackToResultList();
            else {
                throw new CalculatorParseException("Parentheses unpaired");
            }
        }
    }

    private static void operandAction(PolishStackData polishStackData, String curr) {
        polishStackData.addResultList(curr);
    }

    private static void operatorAction(PolishStackData polishStackData, String prev, String curr) throws CalculatorParseException {
        if (!polishStackData.tokenizerHasMoreToken()) {
            throw new CalculatorParseException("Incorrect syntax");
        }
        if (curr.equals(MINUS) && (prev.equals(EMPTY_STRING) || (isDelimiter(prev) && !prev.equals(RIGHT_BRACE)))) {
            operandAction(polishStackData, "0");
        } else {
            while (!polishStackData.isStackEmpty() && (priority(curr) <= priority(polishStackData.peekStack()))) {
                polishStackData.fromStackToResultList();
            }
        }
        polishStackData.pushStack(curr);
    }

    private static void delimiterAction(PolishStackData polishStackData, String curr) throws CalculatorParseException {
        if (curr.equals(LEFT_BRACE)) {
            polishStackData.pushStack(curr);
        } else if (curr.equals(RIGHT_BRACE)) {
            while (!polishStackData.peekStack().equals(LEFT_BRACE)) {
                polishStackData.fromStackToResultList();
                if (polishStackData.isStackEmpty()) {
                    throw new CalculatorParseException("Parentheses unpaired");
                }
            }
            polishStackData.stackPop();
        }
    }

    private static Double calc(List<String> polishString) throws CalculateException {
        Deque<Double> stack = new ArrayDeque<>();
        for (String x : polishString) {
            switch (x) {
                case PLUS:
                    plusAction(stack);
                    break;

                case MINUS:
                    minusAction(stack);
                    break;

                case MULT:
                    multiplicationAction(stack);
                    break;

                case DIVIDE:
                    divisionAction(stack);
                    break;

                default:
                    valueAction(stack, x);
                    break;
            }
        }
        return stack.pop();
    }

    private static void valueAction(Deque<Double> stack, String x) throws CalculateException {
        try {
            stack.push(Double.parseDouble(x));
        } catch (NumberFormatException e) {
            throw new CalculateException("Element parse error");
        }
    }

    private static void divisionAction(Deque<Double> stack) throws CalculateException {
        Double b = stack.pop();
        if (b == 0)
            throw new CalculateException("division by zero");
        Double a = stack.pop();
        stack.push(a / b);
    }

    private static void multiplicationAction(Deque<Double> stack) {
        stack.push(stack.pop() * stack.pop());
    }

    private static void minusAction(Deque<Double> stack) {
        Double b = stack.pop();
        Double a = stack.pop();
        stack.push(a - b);
    }

    private static void plusAction(Deque<Double> stack) {
        stack.push(stack.pop() + stack.pop());
    }

}
